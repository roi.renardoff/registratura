import { showModalLogin } from "../../class/ModalLogin.js"
import { callModal } from "../../class/Modal.js"
import { showDoctor } from "../../class/ModalVisit.js"
import Modal from "../../class/Modal.js";
import { info } from "../cardsFunctions/getInfoFromModal.js";


function closeModal() {
    const modal = document.getElementById('our_modal')
    modal.addEventListener('click', event => {
        if (event.target.dataset.close) {
            const close = new Modal()
            close.close()
        }
    })
}

function login() {
    const buttonLogin = document.getElementById('button_sign')
    buttonLogin.addEventListener('click', event => {
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        showModalLogin()
    });
}

function showModal() {
    const bntModalForm = document.getElementById('button_create')
    bntModalForm.addEventListener('click', () => {
        document.body.style.overflow = ('hidden')
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        callModal()
        showDoctor()
        info()
    });
}

export { login }
export { closeModal }
export { showModal }