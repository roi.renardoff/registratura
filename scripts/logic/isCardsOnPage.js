import { parent } from "../logic/cardsFunctions/cardsWrapperData.js"

const noItemText = document.querySelector('.text__no-items')

function isCardOnPage() {
    if (parent.childElementCount === 0) {
        noItemText.style.display = 'block'
    } else
    if (parent.childElementCount > 0) {
        noItemText.style.display = 'none'
    }
}
export { isCardOnPage }