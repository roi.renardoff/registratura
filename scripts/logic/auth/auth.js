import Request from "../../class/request.js";
import { getAllCards } from "../cardsFunctions/getAllCards.js";
import { getInfo } from "../cardsFunctions/getInfoFromModal.js";

function checkLocalStorage() {
    if (localStorage.getItem('token')) {
        getAllCards()
        const btnLogin = document.getElementById('button_sign')
        btnLogin.classList.add('hide')
        const btnModal = document.getElementById('button_create')
        btnModal.classList.toggle('hide')
    }
}

async function authorization(event) {
    event.preventDefault()
    const email = document.getElementById('input_email').value
    const password = document.getElementById('input_password').value
    const getTokenForAuth = new Request()
    const { data, status } = await getTokenForAuth.getTokenForAuth({ email, password })
    if (status === 200) {
        localStorage.setItem('token', data)
        const btnLogin = document.getElementById('button_sign')
        btnLogin.classList.add('hide')
        const btnModal = document.getElementById('button_create')
        btnModal.classList.toggle('hide')
        const div = document.querySelector('.overlay_modal')
        div.remove()
        getAllCards()
    }
}
const formCreateVisit = document.getElementById("our_modal")

function auth() {
    const divForm = document.getElementById('our_modal')
    if (localStorage.getItem('token')) {
        divForm.removeEventListener('submit', authorization)
    } else {
        formCreateVisit.removeEventListener('submit', getInfo)
        divForm.addEventListener('submit', authorization)
    }
}

export { authorization }
export { auth }
export { checkLocalStorage }