import Request from "../../class/request.js"
import { cardsRender } from "./cardsRender.js"
import { isCardOnPage } from "../isCardsOnPage.js"

async function getAllCards() {
    const getAllCardsToRender = new Request()
    const allCardsToRender = await getAllCardsToRender.getAllCards()
    cardsRender(allCardsToRender)
    isCardOnPage()
}

export { getAllCards }