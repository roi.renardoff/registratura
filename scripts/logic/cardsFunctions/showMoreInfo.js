import { parent } from "./cardsWrapperData.js"

parent.addEventListener("click", showMoreInfo);

function showMoreInfo(event) {
    if (event.target.classList.contains("btn-show-more")) {
        const curentCard = event.target.closest('.card')
        const collapsedDiv = curentCard.querySelector('.card__more-info')
        if (collapsedDiv.classList.contains('collapsed')) {
            event.target.textContent = 'скрыть допольнительную информацию'
            collapsedDiv.classList.remove('collapsed')
        } else {
            event.target.textContent = 'показать больше...'
            collapsedDiv.classList.add('collapsed')
        }
    }
}