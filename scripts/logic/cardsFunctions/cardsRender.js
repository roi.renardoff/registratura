import Card from "../../class/cards.js"
import {parent} from "./cardsWrapperData.js"
import Button from "../../class/button.js"

function cardsRender(cardData) {
  cardData.forEach(item => {

    const card = new Card({
      id: item.id,
      parent: parent,
    })

    card.insertNewElement()

    const curentCardElement = document.getElementById(item.id)

    const delButton = new Button( //classList, id, text, type
      ["btn-delete"],
      "",
      "удалить",
      `${item.id}`,
    ).createButton()
    const editButton = new Button( //classList, id, text, type
      ["btn-edit"],
      "",
      "редактировать",
      `${item.id}`,
    ).createButton()

    setTimeout(() => {
      curentCardElement.append(delButton)
      curentCardElement.append(editButton)
    }, 0)

    card.insertHTML(`<div class="card__top">
                            <span class="top__item hiden" data-item="id" >${item.id}</span>
                            <span class="top__item" data-item="statusCard">${item.statusCard}</span>
                            <span class="top__item" data-item="name">${item.name}</span>
                            <span class="top__item" data-item="doctor">${item.doctor}</span>
                            <span class="top__item" data-item="urgency">${item.urgency}</span>
                            <span class="top__item" data-item="aimVisit">${item.aimVisit}</span>
                            <p class="top__item" data-item="description">${item.description}</p>
                            <button class="btn-show-more">показать больше...</button>
                        </div>
                        <div class="card__more-info collapsed">
                            <span class="more-info__item" data-item="age">${item.age}</span>
                            <span class="more-info__item" data-item="pressure">${item.pressure}</span>
                            <span class="more-info__item" data-item="bmi">${item.bmi}</span>
                            <span class="more-info__item" data-item="disease">${item.disease}</span>
                            <span class="more-info__item" data-item="date">${item.date}</span>
                        </div>`)
  });
  const collectItemsOfCard = document.querySelectorAll(".top__item, .more-info__item")
  collectItemsOfCard.forEach(elem => {
    if (elem.textContent === "undefined") {
      elem.classList.add('hiden')
    }
  })
}


export {cardsRender}