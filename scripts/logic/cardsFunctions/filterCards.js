function searchUrgency(event) {
  const cardCollect = document.querySelectorAll('.card')
  cardCollect.forEach(item => {
    let urgencyForFilter = item.querySelector('[data-item="urgency"]')
    if (event.target.value === 'Все') {
      item.classList.remove('hiden')
    } else if (urgencyForFilter.textContent.includes(event.target.value)) {
      item.classList.remove('hiden')
    } else {
      item.classList.add('hiden')
    }
  })
}

function searchStatus (event) {
  console.log(event.target.value)
  const cardCollect = document.querySelectorAll('.card')
  cardCollect.forEach(item => {
    let statusForFilter = item.querySelector('[data-item="statusCard"]')
    if (event.target.value === 'Все') {
      item.classList.remove('hiden')
    } else if (statusForFilter.textContent.toLowerCase().includes(event.target.value.toLowerCase())){
      item.classList.remove('hiden')
    } else {
      item.classList.add('hiden')
    }
  })
}

function searchInput () {
  document.getElementById('search').oninput = function () {
    let val = this.value.trim()
    let elasticItems = document.querySelectorAll('.card span[data-item="name"]')
    if (val !== '') {
      elasticItems.forEach(function (elem) {
        if (elem.textContent.search(val) === -1) {
          elem.closest('.card').classList.add('hiden')
          elem.innerHTML = elem.textContent
        } else {
          elem.closest('.card').classList.remove('hiden')
          let str = elem.textContent
          elem.innerHTML= insertMark(str, elem.textContent.search(val), val.length)
        }
      })
    } else {
      elasticItems.forEach(function (elem) {
        elem.closest('.card').classList.remove('hiden')
        elem.innerHTML = elem.textContent
      })
    }
  }
  function insertMark(string, pos, len) {
    return string.slice(0, pos) + '<mark>' + string.slice(pos, pos + len) + '</mark>' + string.slice(pos + len)
  }
}

export {searchUrgency}
export {searchStatus}
export {searchInput}

