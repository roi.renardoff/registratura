import Request from "../../class/request.js"
import VisitCardiologist from "../../class/VisitCardiologist.js";
import VisitTherapist from "../../class/VisitTherapist.js"
import VisitDentist from "../../class/VisitDentist.js";
import Modal, { callModal } from "../../class/Modal.js";
import { showDoctor } from "../../class/ModalVisit.js";
import { parent } from "./cardsWrapperData.js"
import { cardsRender } from "./cardsRender.js";
import { isCardOnPage } from "../isCardsOnPage.js";
import { getInfo } from "./getInfoFromModal.js";


let idCard

/// Простите за дублирование кода, не было времени, на этапе рефакторинга мы всё исправим
const formCreateVisit = document.getElementById("our_modal")

function information() {
    const fields = document.querySelectorAll("input, select, textarea")
    const values = {}
    fields.forEach(elem => {
        if (elem.closest('#form')) {
            const { name, value } = elem
            values[name] = value
        }
    })
    console.log(values);
    values.id = idCard
    console.log(values.id)
    return values
}
async function repeatInfo(event) {
    event.preventDefault()
    let infoNewCard
    const addNewCard = new Request()
    const val = information()
    const { id, ...rest } = val
    // тут по наличию id выбирается тип запроса
    infoNewCard = await addNewCard.editCard(id, rest)
        // у обоих запросов в ответе статус и боди, поэтому дальще по общему сценарию
    if (infoNewCard.status === 200) {
        const data = []
        data[0] = infoNewCard.data
        cardsRender(data)
        isCardOnPage()
        document.body.style.overflow = 'scroll'
        const close = new Modal()
        close.close()
    }
}



let idCardEdit = ''
const editCard = new Request()
parent.addEventListener("click", async(event) => {
    if (event.target.classList.contains("btn-edit")) {
        formCreateVisit.removeEventListener('submit', getInfo)
        formCreateVisit.addEventListener('submit', repeatInfo)

        const cardForEdit = event.target.closest('.card') // получаем карточку

        const valesFields = getInfoFromCard(cardForEdit) // получаем обект из полей карты
        idCard = valesFields.id

        document.body.style.overflow = ('hidden')
        const overlay = document.querySelector('.overlay_modal')
        if (overlay) {
            overlay.remove()
        }
        callModal()
        showDoctor()

        const formVisit = document.querySelector('#form')

        const selectDoctor = formVisit.querySelector('#select_doctor')

        const dateCardiologist = new VisitCardiologist()
        const fieldsetCardiologist = dateCardiologist.createFormCardiologist()

        const dateDentist = new VisitDentist()
        const fieldsetDentist = dateDentist.createFormDentist()

        const dateTherapeutic = new VisitTherapist()
        const fieldsetTherapeutic = dateTherapeutic.createFormTherapeutic()

        const extra = document.getElementById('extra')

        if (valesFields.doctor === 'Кардиолог') {
            extra.append(fieldsetCardiologist)
        } else if (valesFields.doctor === 'Терапевт') {
            extra.append(fieldsetTherapeutic)
        } else if (valesFields.doctor === 'Дантист') {
            extra.append(fieldsetDentist)
        }

        for (let key in valesFields) {
            let fild = formVisit.querySelector(`[name="${key}"]`)
            try {
                fild.value = valesFields[key]
            } catch (error) {

            }
        }
        cardForEdit.remove()
    }
});

function getInfoFromCard(cardForEdit) {
    const values = {}
    const cardFields = cardForEdit.querySelectorAll('span, p')

    cardFields.forEach(elem => {
        let key = elem.dataset.item
        let value = elem.textContent
        values[key] = value;
    })
    return values
}

export { idCard }