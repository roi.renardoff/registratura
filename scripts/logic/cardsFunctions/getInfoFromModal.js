import Modal from "../../class/Modal.js";
import Request from "../../class/request.js";
import { cardsRender } from "./cardsRender.js";
import { isCardOnPage } from "../isCardsOnPage.js";
import { idCard } from "./cardEdit.js"

function info() {
    const fields = document.querySelectorAll("input, select, textarea")
    const values = {}
    fields.forEach(elem => {
        if (elem.closest('#form')) {
            const { name, value } = elem
            values[name] = value
        }
    })
    console.log(values);

    return values
}

async function getInfo(event) {
    event.preventDefault()
    let infoNewCard
    const addNewCard = new Request()
    const val = info()
    const { id, ...rest } = val
    // тут по наличию id выбирается тип запроса
    if (id === null || id === undefined || id === '') {
        infoNewCard = await addNewCard.createCard(rest)
        console.log("POST");
        console.log(id);
    } else {
        infoNewCard = await addNewCard.editCard(id, rest)
        console.log("PUT");
        console.log(id);
    }
    // у обоих запросов в ответе статус и боди, поэтому дальще по общему сценарию
    if (infoNewCard.status === 200) {
        const data = []
        data[0] = infoNewCard.data
        cardsRender(data)
        isCardOnPage()
        document.body.style.overflow = 'scroll'
        const close = new Modal()
        close.close()
    }
}

export { info }
export { getInfo }