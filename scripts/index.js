import { renderFilter } from "./class/Filter.js";
import "./logic/cardsFunctions/cardDelete.js" // удаление карточки, событие на "div.card-wrapper"
import { login, showModal, closeModal } from "./logic/modalFunction/funcModal.js";
import { checkLocalStorage } from "./logic/auth/auth.js";
import { searchUrgency, searchStatus, searchInput } from "./logic/cardsFunctions/filterCards.js";
import { auth } from "./logic/auth/auth.js";
import { getInfo } from "./logic/cardsFunctions/getInfoFromModal.js";
import "../scripts/logic/cardsFunctions/cardEdit.js"
import "./logic/cardsFunctions/showMoreInfo.js"


checkLocalStorage()
closeModal()
    // cбор инф с формы
const formCreateVisit = document.getElementById("our_modal")
formCreateVisit.addEventListener("submit", getInfo)
    //authorization/////////////////////////////////////////////////////////////////////////////
auth()
    // event showModalLogin before press button show modal Login
login()

// event showModalFormCard before press button create card
showModal()

// рендерим на странице панель фильтрации карточек
renderFilter()



//filter/////////////////////////////////////////////////////////////////////////////////////
const selectUrgency = document.getElementById('search-urgency')
selectUrgency.addEventListener('change', searchUrgency)
const selectStatus = document.getElementById('search-status')
selectStatus.addEventListener('change', searchStatus);
searchInput()