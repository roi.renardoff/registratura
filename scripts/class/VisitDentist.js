import Visit from "./Visit.js";
import Input from "./Input.js";
import Button from "./Button.js";

export default class VisitDentist extends Visit {
  createFormDentist() {
    const fieldset = document.createElement('fieldset')
    fieldset.classList.add('fieldset_doctor')
    fieldset.id = 'fieldset_doctor'
    const date = new Input('date', 'date_input', 'text', 'select_modal', 'Дата последнего посещения', '').createInput()
    const button = new Button('btn_c', 'create_new_card', 'Создать карточку', 'submit').createButton()
    fieldset.append(date, button)
    return fieldset
  }
}