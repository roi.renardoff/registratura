import Input from "./Input.js";
import Select from "./Select.js";
import Textarea from "./Textarea.js";

export default class Visit {
    CreateVisit() {
        const bodyModal = document.createElement('fieldset')
        bodyModal.classList.add('modal_body')
        const named = new Input(
            'name',
            'input_name_modal',
            'text',
            'select_modal',
            'Веведите ваше имя',
            '')
        const doctor = new Select(
            ['Выберете врача', 'Кардиолог', 'Дантист', 'Терапевт'],
            'doctor',
            'select_doctor',
            () => {},
            'select_modal',
            ''
        )

        const urgency = new Select(
            ['Срочность', 'Обычная', 'Приоритетная', 'Неотложная'],
            'urgency',
            'select_urgency',
            () => {},
            'select_modal',
            ''
        )
        const status = new Select(
            ['Open', 'Done'],
            'statusCard',
            'select_status',
            () => {},
            'select_modal',
            ''
        )
        const purpose = new Input(
            'aimVisit',
            'purpose',
            'text',
            'select_modal',
            'Цель визита',
            ''
        )
        const description = new Textarea(
            'description',
            'desc_modal',
            'select_modal',
            'Краткое описание',
            ''
        )
        bodyModal.append(named.createInput(), doctor.render(), urgency.render(), status.render(), purpose.createInput(), description.createTextarea())
        doctor.hideFirstOption()
        urgency.hideFirstOption()
        return bodyModal
    }
}