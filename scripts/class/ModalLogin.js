import Button from './Button.js'
import Modal from './Modal.js'
import Input from './Input.js'

export default class LoginModal extends Modal {
	createModalLogin() {
		const form = this.createForm()
		const spanEmail = document.createElement('span')
		spanEmail.textContent = 'Email'
		spanEmail.classList.add('span_mod')
		const spanPassword = document.createElement('span')
		spanPassword.textContent = 'Password'
		spanPassword.classList.add('span_mod')
		const extra = document.getElementById('extra')
		const inputForEmail = new Input(
			'email',
			'input_email',
			'email',
			'select_modal',
			'Введите Ваш email',
			''
		).createInput()
		const inputForPassword = new Input(
			'password',
			'input_password',
			'password',
			'select_modal',
			'Введите Ваш пароль',
			''
		).createInput()
		const bntLogin = new Button(
			['btn_f'],
			'btn_login',
			'Login',
			'submit'
		).createButton()
		extra.append(spanEmail, inputForEmail, spanPassword, inputForPassword, bntLogin)
	}
}

function showModalLogin () {
	const create = new LoginModal()
	create.createModalLogin()
	create.open()
}

 export {showModalLogin}