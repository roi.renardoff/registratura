import Input from './input.js'
import Select from './Select.js'

export default class Filter {
  constructor(cards) {
    this.cards = cards
    this.filterCards = null
  }

  render() {
    const filterDiv = document.getElementById('wrapper_filter')
    this.inputSearch = new Input(
      'search-input',
      'search',
      'text',
      'search',
      'Search',
      "",
    )
    this.selectUrgency = new Select(
      ['Urgency', 'Все', 'Обычная', 'Приоритетная', 'Неотложная'],
      'urgency',
      'search-urgency',
      () => {
      },
      ['form-control']
    )
    this.selectStatus = new Select(
      ['Status', 'Все', 'Open', 'Done'],
      'status',
      'search-status',
    )
    filterDiv.append(
      this.inputSearch.createInput(),
      this.selectUrgency.render(),
      this.selectStatus.render()
    )
    this.selectStatus.hideFirstOption()
    this.selectUrgency.hideFirstOption()
  }
}


function  renderFilter() {
  const filter = new Filter()
  filter.render()
}

export {renderFilter}

