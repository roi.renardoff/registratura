import Visit from './Visit.js'
import Input from './Input.js'
import Button from "./Button.js";

export default class VisitCardiologist extends Visit {
  createFormCardiologist() {
    const fieldset = document.createElement('fieldset')
    fieldset.classList.add('fieldset_doctor')
    fieldset.id = 'fieldset_doctor'
    const age = new Input('age', 'age_input', 'text', 'select_modal', 'Возраст', '').createInput()
    const pressure = new Input('pressure', 'pressure_input', 'text', 'select_modal', 'Давление', '').createInput()
    const BMI = new Input('bmi', 'BMI_input', 'text', 'select_modal', 'Индекс массы тела', '').createInput()
    const disease = new Input('disease', 'disease_input', 'text', 'select_modal', 'Перенесенные заболевания', '').createInput()
    const button = new Button('btn_c', 'create_new_card', 'Создать карточку', 'submit').createButton()
    fieldset.append(age, pressure, BMI, disease, button)
    return fieldset
  }
}