import Modal from './Modal.js'
import VisitCardiologist from './VisitCardiologist.js'
import VisitDentist from './VisitDentist.js'
import VisitTherapist from './VisitTherapist.js'

export default class ModalVisit extends Modal {
  showFormDoctor() {
    const form = document.getElementById('form')
    const extra = document.getElementById('extra')

    const dateCardiologist = new VisitCardiologist()
    const fieldsetCardiologist = dateCardiologist.createFormCardiologist()

    const dateDentist = new VisitDentist()
    const fieldsetDentist = dateDentist.createFormDentist()

    const dateTherapeutic = new VisitTherapist()
    const fieldsetTherapeutic = dateTherapeutic.createFormTherapeutic()

    form.addEventListener('change', function (event) {
      if (event.target.value === 'Кардиолог') {
        const fDoctor = document.querySelector('.fieldset_doctor')
        if(fDoctor){
          fDoctor.remove()
        }
        extra.append(fieldsetCardiologist)
      } else if (event.target.value === 'Дантист') {
        const fDoctor = document.querySelector('.fieldset_doctor')
        if(fDoctor){
          fDoctor.remove()
        }
        extra.append(fieldsetDentist)
      } else if (event.target.value === 'Терапевт') {
        const fDoctor = document.querySelector('.fieldset_doctor')
        if(fDoctor){
          fDoctor.remove()
        }
        extra.append(fieldsetTherapeutic)
      }
    });
  }
}

function showDoctor () {
  const a = new ModalVisit()
  a.showFormDoctor()
}

export {showDoctor}

