export default class Card {
  constructor({id, classname = 'card', parent}) {
    this.id = id, this.parent = parent, this.classname = classname
  }
  insertNewElement() {
    let newElem = document.createElement('div');
    newElem.className = this.classname;
    newElem.setAttribute('id', this.id);
    this.parent.insertAdjacentElement("beforeend", newElem);
    this.newElem = newElem;
  }
  insertHTML(strHtml) {
    this.newElem.innerHTML = strHtml;
  }
}