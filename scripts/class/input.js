export default class Input {
  _name = undefined
  _id = undefined;
  _className = undefined;
  _placeholder = undefined;
  _value = undefined;

  constructor(name, id, type, className, placeholder, value, handlerChange) {
    this._name = name
    this._id = id;
    this._type = type;
    this._className = className;
    this._placeholder = placeholder;
    this._value = value;
    this.handlerChange = handlerChange;
  }

  createInput() {
    const input = document.createElement("input");
    input.classList.add(this._className);
    input.setAttribute("id", this._id);
    input.setAttribute("type", this._type);
    input.setAttribute("placeholder", this._placeholder);
    input.setAttribute("name", this._name);
    if (this._type === "submit") {
      input.setAttribute("value", this._value);
      return input;
    } else {
      input.setAttribute("required", "");
    }
    input.addEventListener('change', this.handlerChange);
    return input;
  }

  createWrapper() {
    const wrapper = document.createElement("p");
    wrapper.classList.add("mb-2");
    return wrapper;
  }
}
