export default class Textarea {
    constructor(name, id, className, placeholder, value) {
        this.name = name
        this.id = id
        this.className = className
        this.placeholder = placeholder
        this.value = value
    }
    createTextarea() {
        const textArea = document.createElement('textarea')
        textArea.name = this.name
        textArea.id = this.id
        textArea.classList.add(this.className)
        textArea.setAttribute('placeholder', this.placeholder)
        return textArea
    }
}