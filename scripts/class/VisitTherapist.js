import Visit from './Visit.js'
import Input from './Input.js'
import Button from "./Button.js";

export default class VisitTherapeutic extends Visit {
  createFormTherapeutic() {
    const fieldset = document.createElement('fieldset')
    fieldset.classList.add('fieldset_doctor')
    fieldset.id = 'fieldset_doctor'
    const age = new Input('age', 'date_input', 'text', 'select_modal', 'Возраст', '').createInput()
    const button = new Button('btn_c', 'create_new_card', 'Создать карточку', 'submit').createButton()
    fieldset.append(age, button)
    return fieldset
  }
}