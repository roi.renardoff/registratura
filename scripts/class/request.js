import instance from "../logic/router/instance.js"

const Api_Token = '952c0ec2-a2b9-4adc-a2d5-e948edf505f7'
export default class Request {
  constructor(password = 'qwerty123456', email = 'mytoken@gmail.com', token = Api_Token) {
    this.password = password
    this.email = email
    this.token = token
  }

  async createCard(dataBody) {
    const {status, data} = await instance.post(' ', dataBody, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.token}`,
      }
    })
    return {status, data}
  }

  async getAllCards() {
    const {data} = await instance.get(' ', {
      headers: {Authorization: `Bearer ${this.token}`}
    })

    return data
  }

  async getCard(id) {
    const {data} = await instance.get(`${id}`, {
      headers: {Authorization: `Bearer ${this.token}`}
    })
    console.log(data)
    return data
  }

  async deleteCard(id) {
    let stat
    await instance
      .delete(`${id}`, {
        headers: {Authorization: `Bearer ${this.token}`}
      })
      .then(({status}) => {
        stat = status
      })
    return stat
  }

  async editCard(id, dataBody) {
    const {status, data} = await instance.put(`${id}`, dataBody, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.token}`,
      }
    })
    return {status, data}
  }

  async checkCards() {
    const {data} = await instance.get(' ', {
      headers: {
        Authorization: `Bearer ${this.token}`,
      }
    })
    return data.length
  }

  async getTokenForAuth(body) {
    const {data, status} = await instance.post('https://ajax.test-danit.com/api/v2/cards/login', body)
      .catch(err => {
        alert('Неправильный пароль')
      })
    return {data, status}
  }
}
//